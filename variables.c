/**
 * @file variables.c
 * @author Aureliano Buendia
 * @brief 
 * @version 0.1
 * @date 2021-11-13
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <stdio.h>

int main(void){
    int height, length, width, volume, weight; /* Declaracion de variables*/
    float profit, loss;

    height = 8; /* Se le asignan valores a las variables */
    length = 12;
    width = 10;

    profit = 2150.48f; /* Es mejor ponerle la f al final de un valor (float) */

    volume = height * length * width;
    weight = (volume + 165) / 166;

    printf("Dimensiones: %dx%dx%d\n", height, length, width);
    printf("Volumen: (pulgadas cubicas): %d\n", volume);
    printf("Peso dimensional(libras): %d\n", weight);

    printf("Height: %d\n", height); /* %d es donde va terminar el valor de height, %d solo funciona para valores de tipo int */
    printf("Profit: $%.2f\n", profit); /* %f es donde va a terminar el valor de profit, por defecto %f trae seis digitos despues del deciman para traer n digitos (%.2f) donde 2 es n */
    printf("Height: %d, Length: %d\n", height, length);
    
    return 0;
}
