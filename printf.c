/**
 * @file printf.c
 * @author Aureliano Buendia
 * @brief 
 * @version 0.1
 * @date 2021-11-13
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <stdio.h> /* (Directivas) Importamos libreriar standart i/o (input/output) */

/* Asi se hace un comentario*/

/* Como pintar algo en la pantalla con C. */
int main(void) /* (Funcion) */
{
    printf("To c, or not to c: that's the question!\n"); /* (Declaraciones) */
    printf("To c, or not to c:"); /* (Declaraciones) */
    printf("that's the question!\n"); /* (Declaraciones) */
    printf("Brevity is the soul of wit \n --Shakespeare\n"); /* (Declaraciones) */
    return 0; /* Le indica al programa que retorne 0 a la maquina. */
}
