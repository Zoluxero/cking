/**
 * @file scanf.c
 * @author Aureliano Buendia
 * @brief 
 * @version 0.1
 * @date 2021-11-14
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <stdio.h>

int main(void)
{
    int height, length, width, volume, weight;
    printf("Ingrese el alto de la caja: ");
    scanf("%d", &height);
    printf("Ingrese el largo de la caja: ");
    scanf("%d", &length);
    printf("Ingrese el ancho de la caja: ");
    scanf("%d", &width);
    volume = height * length * width;
    weight = (volume + 165) / 166;

    printf("Volumen (Pulgadas cubicas): %d\n", volume);
    printf("Peso dimensional (libras): %d\n", weight);

    return 0;  
}